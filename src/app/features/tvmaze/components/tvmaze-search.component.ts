import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { debounceTime, distinctUntilChanged, filter, map, mergeMap } from 'rxjs/operators';
import { Series } from '../../../model/series';

@Component({
  selector: 'app-tvmaze-search',
  template: `
    <input type="text" [formControl]="inputSearch" class="search-input">

  `,
  styles: [`
    .search-input {
      /*font-family: 'Roboto', sans-serif;*/
      color: #333;
      font-size: 1.8rem;
      margin: 0 auto ;
      padding: 1.5rem 1.5rem;
      border-radius: 1.2rem;
      background-color: rgb(222, 222, 222);
      border: none;
      width: 100%;
      max-width: 600px;
      display: block;
      border-bottom: 0.3rem solid transparent;
      transition: all 0.3s;
      outline: none;
    }

  `]
})
export class TvmazeSearchComponent implements OnInit {
  @Output() search: EventEmitter<Series[]> = new EventEmitter<Series[]>();
  inputSearch: FormControl = new FormControl();

  constructor(private http: HttpClient) {
    this.inputSearch.valueChanges
      .pipe(
        map(text => text.toLowerCase()),
        filter(text => text.length > 2),
        debounceTime(1000),
        distinctUntilChanged(),
        mergeMap(text => this.http.get<Series[]>(`http://api.tvmaze.com/search/shows?q=${text}`)),
        // ...
      )
      .subscribe(result => {
        this.search.emit(result)
      });

    // this.inputSearch.setValue('grey')
  }

  ngOnInit(): void {
  }

}
