import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div
        class="card-header"
        (click)="toggle()"
        [ngClass]="{
          'bg-success': type === 'success',
          'bg-danger': type === 'failed'
        }"
        *ngIf="title"
      >
        {{title}}
        <i *ngIf="icon"
           (click)="iconClickHandler($event)"
           class="pull-right" [ngClass]="icon"></i>
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
    
  `,
})
export class CardComponent {
  @Input() title: string;
  @Input() icon: string;
  @Input() type: 'success' | 'failed';
  @Output() iconClick = new EventEmitter();
  opened = true;

  toggle(): void {
    this.opened = !this.opened;
  }

  iconClickHandler(event: MouseEvent): void {
    event.stopPropagation();
    this.iconClick.emit();
  }
}


