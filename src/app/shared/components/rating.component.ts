import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  template: `
    <div
      *ngIf="value"
      [style.width.px]="value * 10"
      [style.background-color]="value < 7 ? 'orange' : 'green'"
      class="rating"
    >
      {{value}}
    </div>
  `,
  styles: [`

    .rating {
      border-radius: 20px;
      text-align: center;
      padding: 5px;
      margin: 0 auto;
      color: white;
    }
  `]
})
export class RatingComponent {
  @Input() value: number; // 0 - 10

}
