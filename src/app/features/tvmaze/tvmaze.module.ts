import { NgModule } from '@angular/core';
import { TvmazeComponent } from './tvmaze.component';
import { TvmazeResultsComponent } from './components/tvmaze-results.component';
import { TvmazeModalComponent } from './components/tvmaze-modal.component';
import { TvmazeSearchComponent } from './components/tvmaze-search.component';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HelpComponent } from './components/help/help.component';

@NgModule({
  declarations: [
    TvmazeComponent,
      TvmazeResultsComponent,
      TvmazeModalComponent,
      TvmazeSearchComponent,
      HelpComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: '', component: TvmazeComponent},
      { path: 'help', component: HelpComponent},
    ])
  ]
})
export class TvmazeModule {

}
