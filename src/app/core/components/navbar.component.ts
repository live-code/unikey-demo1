import { Component } from '@angular/core';
import { UserService } from '../../features/users/services/user.service';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-navbar',
  template: `
    
    <div 
      class="mynavbar"
      [ngClass]="{
        'dark': themService.value === 'dark',
        'light': themService.value === 'light'
      }"
    >
      <button [routerLink]="'login'" routerLinkActive="bg-warning">login</button>
      <button routerLink="home" routerLinkActive="bg-warning">home</button>
      <button routerLink="users" routerLinkActive="bg-warning">users</button>
      <button routerLink="tvmaze" routerLinkActive="bg-warning">tvmaze</button>
      <button routerLink="contacts" routerLinkActive="bg-warning">contacts</button>
      <button routerLink="settings" routerLinkActive="bg-warning">settings</button>
    </div>
  `,
  styles: [`
      .mynavbar {
        padding: 20px;
      }
    .dark { background-color: #222; color: white }
    .light { background-color: #dedede}
  `]
})
export class NavbarComponent {
  constructor(public themService: ThemeService) {}
}
