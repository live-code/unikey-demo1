import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-list',
  template: `
    <li
      class="list-group-item"
      *ngFor="let user of items"
      [routerLink]="'/users/' + user.id"
      [ngClass]="{ 
            'male': user.gender === 'M', 
            'female': user.gender === 'F'
          }"
    >
      <app-gender-icon [value]="user.gender"></app-gender-icon>
      {{user.name}}
      <i
        class="fa fa-trash pull-right"
        [style.color]="user.gender === 'M' ? 'white' : null"
        (click)="deleteHandler(user, $event)"
      ></i>
    </li>
  `,
  styles: [
  ]
})
export class UsersListComponent {
  @Input() items: Partial<User>[];
  @Output() deleteUser: EventEmitter<Partial<User>> = new EventEmitter();

  deleteHandler(user: Partial<User>, event: MouseEvent): void {
    event.stopPropagation();
    this.deleteUser.emit(user);
  }
}
