import { Component } from '@angular/core';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-users',
  template: `
    <div class="row">
      <div class="my-column">
        <app-card title="USER FORM">
          <app-users-form
            (save)="userService.save($event)"
          ></app-users-form>
        </app-card>
      </div>

     <div 
       class="my-column"
       [ngClass]="{
        'hide': !userService.users?.length
       }"
     >
       <app-card [title]="userService.users?.length ? userService.users?.length + ' users' : 'NO USERS'">
         <app-users-list 
           [items]="userService.users"
           (deleteUser)="userService.deleteHandler($event)"
         ></app-users-list>
       </app-card>
     </div>
    </div>
     
  `,
  styles: [`
    .my-column {
      transition: 1s all ease-in-out;
      width: 50%;
      overflow: hidden;
    }
    .hide {
      width: 0;
    }
  `]
})
export class UsersComponent {
  constructor(public userService: UserService) {
    this.userService.getUsers();
  }
}

