import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item"
          *ngFor="let item of items"
          (click)="tabClick.emit(item)"
      >
        <a class="nav-link" [ngClass]="{'active': item.id === active?.id}">
          {{item.label}} - {{item.id}} - {{active?.id}}
        </a>
      </li>
    </ul>
  `,
  styles: [
  ]
})
export class TabbarComponent  {
  @Input() items: any[];
  @Output() tabClick: EventEmitter<any> = new EventEmitter();
  @Input() active: any;
}
