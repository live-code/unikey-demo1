import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabbarComponent } from './components/tabbar.component';
import { HelloComponent } from './components/hello.component';
import { CardComponent } from './components/card.component';
import { RatingComponent } from './components/rating.component';

@NgModule({
  declarations: [
    RatingComponent,
    HelloComponent,
    CardComponent,
    TabbarComponent,

  ],
  imports: [
    CommonModule
  ],
  exports: [
    RatingComponent,
    HelloComponent,
    CardComponent,
    TabbarComponent,
  ]
})
export class SharedModule { }
