export type User = {
  id: number;
  name: string;
  age: number;
  gender: string;
  city: string;
  birthday: number;
  bitcoins: number;
};

export type UserAdd = Pick<User, 'id' | 'name' | 'gender' | 'city'>;
